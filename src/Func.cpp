#include "SomeFunction.hpp"
namespace ek {

    void Swap(int& a, int& b) // ��������� ������ ���� ����������
    {
        int tmp = a;
        a = b;
        b = tmp;
    }

    int SumMaxDigit(int n, int Matrix[N][N]) // ������� ���� ������������ ������� �������
    {
        int max_digit = Matrix[0][0];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                if (max_digit < Matrix[i][j])
                    max_digit = Matrix[i][j];
        int sum1 = 0;
        while (max_digit > 0)               // � ������� ����� ���� ����� ��������
        {
            sum1 += max_digit % 10;
            max_digit /= 10;
        }
        return sum1;
    }

    int SumMinDigit(int n, int Matrix[N][N]) //������� ���� ����������� ������� �������
    {
        int min_digit = Matrix[0][0];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                if (min_digit > Matrix[i][j])
                    min_digit = Matrix[i][j];
        int sum2 = 0;
        while (min_digit > 0)                // � ������� ����� ���� ����� ��������
        {
            sum2 += min_digit % 10;
            min_digit /= 10;
        }
        return sum2;
    }

    bool Equal(int sum1, int sum2) // ��������� ���� �����
    {
        if (sum1 == sum2)
        {
            return true;
        }
        return false;
    }

    int Sum(int n, int* Mas) // ����� ��������� ������
    {
        int sum_s = 0;
        for (int i = 0; i < n; i++)
            sum_s += Mas[i];
        return sum_s;
    }

    void Sort(int n, int Matrix[N][N]) // ���������� ������� �� ����� ��������� ������
    {
        for (int j = 0; j < n; j++)
            for (int k = j + 1; k < n; k++)
            {
                if (Sum(n, Matrix[j]) > Sum(n, Matrix[k]))
                    for (int i = 0; i < n; i++)
                        Swap(Matrix[j][i], Matrix[k][i]);
            }
    }
}
