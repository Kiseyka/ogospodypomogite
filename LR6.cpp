﻿#include <iostream>
#include "SomeFunction.hpp"


/*Дана целочисленная матрица {Aij}i=1...n;j=1..n , n<=100. 
Если суммы цифр минимального и максимального элементов матрицы одинаковы,
упорядочить строки матрицы по неубыванию суммы элементов.
*/

void Read(int& n, int Matrix[N][N]) // Считываем матрицу
{
    std::cin >> n;
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            std::cin >> Matrix[i][j];
}

void Write(int n, int Matrix[N][N]) // Записываем результат
{
    std::cout << n << std::endl;
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
            std::cout << Matrix[i][j] << " ";
         std::cout << std::endl;
    }
        
}

int main()
{
    int n;
    int Matrix[N][N];
    Read(n, Matrix); // Считываем матрицу
    if (ek::Equal(ek::SumMinDigit(n, Matrix), ek::SumMaxDigit(n, Matrix))) // Сравниваем суммы цифр минимального и максимального элементов
        ek::Sort(n, Matrix); // Сортируем матрицу
    Write(n, Matrix); // Выводим матрицу
    return 0;
}