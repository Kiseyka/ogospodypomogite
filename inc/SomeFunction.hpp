#pragma once
#include <math.h>

namespace ek
{
	#define N 100

	void Swap(int& a, int& b);// ����� ���� ����������

	int SumMaxDigit(int n, int Matrix[N][N]); // ����� ���� ������������ �������� ������

	int SumMinDigit(int n, int Matrix[N][N]); // ����� ���� ������������ �������� ������

	bool Equal(int sum1, int sum2); // ���������� ��� ��������

	int Sum(int n, int* Mas); // ����� ��������� ������

	void Sort(int n, int Matrix[N][N]); // ���������� ������� �� ����� ��������� ������
}

